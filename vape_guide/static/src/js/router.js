document.addEventListener("DOMContentLoaded", function (event) {
	(function () {
		let setViewByHash = function () {
			let hash = location.hash.substring(1);
			if (!document.getElementById(hash)) {
				//static view not exists yet
				let a=document.querySelector('a[href="#'+hash+'"]'),
				path=a.getAttribute('data-path'),
				to=a.getAttribute('data-to'),
				parent = document.getElementById(to),
				newView = document.createElement('section');
				newView.id = hash;
				newView.innerHTML = "pleas wait for a sec...";
				parent.appendChild(newView);
				if (!path.includes('.html'))
					path += '.html';

				fetch(path).then(function (response) {
					// When the page is loaded convert it to text
					return response.text()
				}).then(function (html) {
					//set view
					newView.innerHTML = html;

				}).catch (function (err) {
					console.log('Failed to fetch page: ', err);
					newView.innerHTML = "Network problem";
				});

			}
			let activeView = document.getElementById(hash),
			viewToActive = function (el) {
				if (el.tagName != "BODY") {
					let siblings = el.parentNode.childNodes,
					i = 0;
					[...siblings].forEach(function (sibling, i) {
						if (sibling.tagName == 'SECTION')
							sibling.classList.remove('active');
					});
					//hashEl is now active
					el.classList.add('active');
					return true;
				} else {
					return false;
				}
			};
			//update view
			if (typeof(activeView) != 'undefined' && activeView != null) {
				while (viewToActive(activeView))
					activeView = activeView.parentNode;
			}

		};
		//set up view first time
		if (!location.hash){
		document.querySelector('a[href^="#"]').click();
		}else{
			setViewByHash();
		}
		//change view
		window.addEventListener('hashchange', setViewByHash, false);
	})();
});
