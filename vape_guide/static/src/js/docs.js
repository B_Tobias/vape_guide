document.addEventListener("DOMContentLoaded", function (event) {
	(function () {
		let nav=document.getElementById('docsNav'),
			docsShowNav=document.getElementById('docsShowNav'),
			onMobile=false;
		
		docsShowNav.addEventListener('click',function(){
			onMobile=true;
			if (nav.style.display!="block")
				nav.style.display="block";
			else
				nav.style.display="none";
		});
		window.addEventListener('hashchange', function(){
			if (onMobile)
				nav.style.display="none";
		}, false);
	})();
});
