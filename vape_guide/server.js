const express=require('express');
const fs = require('fs');
const path = require('path');

const app = express();

//append docs links to index
let indexPath= __dirname+'/app/index.html',
articlePath=__dirname + '/static/article/',
//index.html to var
indexContent=fs.readFileSync(indexPath,'utf8'),

//article group-names to array
dirNames=fs.readdirSync(articlePath),

//add links to index.html
linkList='',
replaceSpace=str=>str.replace(new RegExp('_', 'g'), ' '),
removeAccent=function(str){
	return str.replace(/[AÁ]/g,"A")
	.replace(/[aá]/g,"a")
	.replace(/[É]/g,"E")
	.replace(/[é]/g,"e")
	.replace(/[ÜŰÚ]/g,"U")
	.replace(/[üűú]/g,"u")
	.replace(/[ŐÓÖ]/g,"O")
	.replace(/[öóő]/g,"o")
	.replace(/[Í]/g,"I")
	.replace(/[í]/g,"i");
};
dirNames.map(function(dir,i){
	//group title
	linkList+='<span class="groupTitle">'+replaceSpace(dir)+'</span>';
	//links for group
	fs.readdirSync(articlePath+'/'+dir).map(function(articleName,i){
		linkList+=`<a class="navLink"
			href="#${removeAccent(articleName.substring(0,articleName.length-5))}"
			data-to="docsContent"
			data-path="/article/${dir}/${articleName}">
				${replaceSpace(articleName).substring(0,articleName.length-5)}
			</a>`;
	});
});
indexContent=indexContent.replace('linksToContent',linkList);

//serving data for fetch
app.use(express.static(__dirname + '/static'));

app.get('*', (req, res,next) =>{
	res.set('Content-Type', 'text/html');
	res.send(new Buffer(indexContent));
});


app.listen(3000, () => console.log(`Example app listening on port 3000!`));